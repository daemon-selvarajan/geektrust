import unittest
from mock import MagicMock,patch
from geektrust import GoldenCrown


class TestGoldenCrown(unittest.TestCase):

    @patch('geektrust.GoldenCrown.file_parser', return_value=[('AIR', 'ROZO'), ('AIR', 'ROZO'), ('AIR', 'ROZO')])
    def test___init__(self, file_parser_patch):
        mock_object = MagicMock(spec=GoldenCrown)
        GoldenCrown.__init__(mock_object, input_file='mocked_file_name.txt')
        self.assertEqual('NONE', mock_object.result)
        GoldenCrown.file_parser.assert_called_with('mocked_file_name.txt')

    @patch('builtins.open')
    def test_file_parser(self, open_patch):
        mocked_open_object = MagicMock()
        open_patch.return_value = mocked_open_object
        self.assertEqual([],GoldenCrown.file_parser('mocked_file_name.txt'))

    def test_backward(self):
        self.assertEqual('A', GoldenCrown.backward(letter='D', jump=3))
        self.assertEqual('B', GoldenCrown.backward(letter='E', jump=3))
        self.assertEqual('C', GoldenCrown.backward(letter='F', jump=3))
        self.assertEqual(' ', GoldenCrown.backward(letter=' ', jump=3))

    def test_decrypt(self):
        self.assertEqual('OLWL', GoldenCrown.decrypt(message='ROZO', key=3))
        self.assertEqual('AYDSSNJFFJAWWHP', GoldenCrown.decrypt(message='FDIXXSOKKOFBBMU', key=5))

    def test_war_status(self):
        self.assertEqual(True, GoldenCrown.war_status(kingdom='AIR', encrypted_message='ROZO'))
        self.assertEqual(True, GoldenCrown.war_status(kingdom='AIR', encrypted_message='ROZOL'))
        self.assertEqual(True, GoldenCrown.war_status(kingdom='WATER', encrypted_message='OCTO VJAVWBZ PUS'))
        self.assertEqual(False, GoldenCrown.war_status(kingdom='ICE', encrypted_message='MAMMOTH THVAO'))
        self.assertEqual(False, GoldenCrown.war_status(kingdom='FIRE', encrypted_message='DRAGON JT'))

    def test_run(self):
        mock_object = MagicMock(spec=GoldenCrown)
        mock_object.result = 'NONE'
        mock_object.file_content = [('LAND', 'PANDAUFSI'), ('ICE', 'MAMMOTH THVAO'),
                                    ('FIRE', 'DRAGON JXGMUT'), ('AIR', 'ZORRO'), ('WATER', 'OCTO VJAVWBZ PUS')]
        GoldenCrown.run(mock_object)
        self.assertEqual('SPACE FIRE AIR WATER', mock_object.result)

        mock_object = MagicMock(spec=GoldenCrown)
        mock_object.result = 'NONE'
        mock_object.file_content = [('AIR', 'ROZO'), ('AIR', 'ROZO'), ('AIR', 'ROZO')]
        GoldenCrown.run(mock_object)
        self.assertEqual('NONE', mock_object.result)


if __name__ == '__main__':
    unittest.main()
