#!/usr/bin/env python

"""
A golden crown ::
This Program will take the location to the test file as parameter. Input is to be read from a text file,
and output will be printed to the console.
"""

__author__ = "Selvarajan Thanabal"
__credits__ = ["geektrust.in"]
__license__ = "OPEN"
__version__ = "1.0.0"
__maintainer__ = "Selvarajan Thanabal"
__email__ = "nousewithlatenews@gmail.com"
__status__ = "Production"
__date__ = "March 27, 2020"
__contact__ = "+91 86080 17060"

import sys
from collections import Counter


class GoldenCrown(object):

    CIPHER_KEY = {'AIR': 'owl', 'LAND': 'panda', 'ICE': 'mammoth', 'FIRE': 'dragon', 'WATER': 'octopus'}

    def __init__(self, input_file):
        """
        Initializes class with input_file
        :param input_file: (str) Absolute path to input file
        Input file content format should be space separated <KINGDOM> <ENCRYPTED_MESSAGE>
        Example: sample_input.txt
           AIR ROZO
           LAND FAIJWJSOOFAMAU
           ICE STHSTSTVSASOS
        """
        self.file_content = GoldenCrown.file_parser(input_file)
        self.result = 'NONE'

    @staticmethod
    def file_parser(file):
        """
        Static method which parses the file content and returns list of tuples
        :param file: (str) file path
        :return: (list) List of tuples
        """
        with open(file, 'r') as infile:
            file_content = list(map(lambda x: tuple(x.split(' ', maxsplit=1)),
                                    infile.readlines()))
        return file_content

    @staticmethod
    def backward(letter, jump):
        """
        Backward jumps from the given letter with the given jump count
        :param letter: (str) alphabet character
        :param jump: (int) jump count
        :return: (str) decrypted character
        """
        if letter == ' ': return letter
        start_character = ord('A')
        start = ord(letter) - start_character
        offset = ((start - jump) % 26) + start_character
        result = chr(offset)
        return result

    @staticmethod
    def decrypt(message, key):
        """
        Decrypts the given encrypted message with the given key
        :param message: (str) Encrypted message
        :param key: (int) backward jump count from each character
        :return: (str) decrypted message
        """
        decrypted_message = ''
        for letter in message:
            decrypted_message += GoldenCrown.backward(letter=letter, jump=key)
        return decrypted_message

    @staticmethod
    def war_status(kingdom, encrypted_message):
        """
        Returns True if the kingdom key ( CIPHER_KEY ) characters present in the given message after the decryption
        :param kingdom: (str) kingdom name
        :param encrypted_message: (str) encrypted message
        :return: (bool) True or False
        """
        key = GoldenCrown.CIPHER_KEY[kingdom]
        decrypted = GoldenCrown.decrypt(message=encrypted_message, key=len(key))
        substring_counts = Counter(key.upper())
        text_counts = Counter(decrypted)
        return all(text_counts[letter] >= count for letter, count in substring_counts.items())

    def run(self):
        """
        This is the trigger method
        Parses the file content and decides whether 'SPACE' kingdom wins atleast 3 or more other kingdoms &
        prints the results in the following format
        <RULER_KINGDOM> <ALLY_KINGDOM_1 ALLY_KINGDOM_N>
        Example:
        SPACE AIR LAND
        :return: None
        """
        result = ['SPACE']
        count = 0
        for kingdom, encrypted_message in self.file_content:
            if kingdom not in result and GoldenCrown.war_status(kingdom=kingdom, encrypted_message=encrypted_message):
                result.append(kingdom)
                count += 1
        if count >= 3:
            self.result = " ".join(result)
        print(self.result)


if __name__ == '__main__':
    tame = GoldenCrown(input_file=sys.argv[1])
    tame.run()

