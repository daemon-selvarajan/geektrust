## Quick Help 

| **Field** | **Description** |
| ------ | ------ |
| **Name** | geektrust |
| **File** | GeekTrustSite/TameofThrones/geektrust.py | 
| **Installation** | pip install -r requirement.txt  |
|   **Usage**| python -m geektrust input_file.txt |
| **pydoc** | pydoc geektrust |
| **unittest** | python test_geektrust.py |
| **code coverage** | ./test.sh |


## Meta Data

    __author__ = 'Selvarajan Thanabal'  
    __author__ = 'Selvarajan Thanabal'
    __credits__ = ['geektrust.in']
    __email__ = 'nousewithlatenews@gmail.com'
    __license__ = 'OPEN'
    __maintainer__ = 'Selvarajan Thanabal'
    __status__ = 'Production'
    __version__ = '1.0.0'

## Version
1.0.0

## Author
Selvarajan Thanabal

## Credits
['geektrust.in']


