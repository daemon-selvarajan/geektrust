# GeekTrust

[GeekTrustSite](https://www.geektrust.in/login) is a code review platform.  You validate your code’s industry readiness. 

# Your code is your resume
Great coders write great code, not great resumes. Today, companies start the hiring process with your code. Solve any one Geektrust challenge to bypass coding rounds with multiple companies. Get feedback from our senior dev team. Show off your coding skills and get companies to show some serious interest in you.

# How GeekTrust evaluates?

Please refer geektrust [EvaluationCriteria](https://www.geektrust.in/how-we-evaluate) page

# Repository content

This repo consist of the problems solved for geektrust 


